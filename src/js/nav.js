$(document).ready(function() {
	let hamburger = $('nav.nav .hamburger');
	let menu = $('nav.nav .menu');
	let menuLink = $('nav.nav .menu ul a');

	function headerAnimation(offset) {
		let nav = $('nav.nav');
		let scrollTop = $(window).scrollTop();

		if (scrollTop > offset) {
			nav.addClass('scroll');
		} else {
			nav.removeClass('scroll');
		}
	}

	hamburger.click(function(){
		if (hamburger.hasClass('open')) {
			hamburger.removeClass('open');
			menu.removeClass('open');
		} else {
			hamburger.addClass('open');
			menu.addClass('open');
		}
	});

	menuLink.click(function(){
		hamburger.removeClass('open');
		menu.removeClass('open');
	});

	$(window).scroll(function() {
		headerAnimation(100);
	});
});