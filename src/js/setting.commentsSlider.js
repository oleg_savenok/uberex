$(function() {
	$('.comments-slider').bxSlider({
		//prevSelector: '#prev-comments',
		//nextSelector: '#next-comments',
		prevText: "",
		nextText: "",
		pager: true,
    pagerType: 'short',
		minSlides: 1,
		maxSlides: 1,
		moveSlides: 1,
		maxHeight: 300,
		adaptiveHeight: false,
		slideWidth: 5000,
		slideMargin: 10,
		infiniteLoop: true,
		easing: 'easeOutQuint'
	});
});