$(document).ready(function(){
	$('a.anchor').click(function(){
		let windowHeight = $(window).height();
		let anchorMargin;

		if (windowHeight > 800) {
			anchorMargin = 150;
		} else if (600 > windowHeight > 800) {
			anchorMargin = 100;
		} else {
			anchorMargin = 75;
		};

		$('html, body').animate({
			scrollTop: ($('[name="' + $.attr(this, 'href').substr(1) + '"]').offset().top) - anchorMargin
		}, 1000);
		return false;
	});
});