$(function() {
	function scrollToTop(params) {
		$(document).scrollTop(0);
	};

	$("html").niceScroll({
		zindex: 5000,
		cursorcolor: "#333",
		cursoropacitymin: 0.25,
		cursoropacitymax: 1,
		cursorwidth: "10px",
		cursorborder: "0px",
		cursorborderradius: "0px",
		scrollspeed: 40,
		mousescrollstep: 30,
		hidecursordelay: 250,
	});

	setTimeout(scrollToTop, 200);
});