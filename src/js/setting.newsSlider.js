$(function() {
	let windowWidth;

	function bxSlideCount(count, margin) {
		$('.news-slider').bxSlider({
			prevSelector: '#prev-news',
			nextSelector: '#next-news',
			preventDefaultSwipeY: true,
			prevText: "",
			nextText: "",
			pager: false,
			minSlides: count,
			maxSlides: count,
			moveSlides: 1,
			maxHeight: 300,
			adaptiveHeight: false,
			slideWidth: 5000,
			slideMargin: margin,
			infiniteLoop: true,
			easing: 'easeOutQuint'
		});
	};

	function bxSliderSetting() {
		windowWidth = $(window).width();

		if (windowWidth > 1200) {
			bxSlideCount(3, 60);
		} else if (1200 > windowWidth && windowWidth > 768) {
			bxSlideCount(2, 20);
		} else if (768 > windowWidth) {
			bxSlideCount(1, 20);
		}
	};

	bxSliderSetting();
});