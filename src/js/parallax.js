$(document).ready(function() {
	function parallax(target, multiplier, info) {
		const parallaxImage = $(target + " img.parallax-img");
		const parent = $(target);
		const content = $(target + " .content");

		let offset = $(target).offset();
		let offsetTop = offset.top;

		let scrollTop = $(window).scrollTop();
		let windowHeight = $(window).height();

		let margin;

		if (windowHeight > 1600) {
			margin = 250;
		} else {
			margin = 150;
		};

		parent.css({
			"height": content.height() + margin
		});

		parallaxImage.css({
			"margin-top": 0
		});

		let imageHeight = parallaxImage.height();
		let parentHeight = parent.height();

		let maxParallaxDistanse = imageHeight - parentHeight;
		let distanseStep = (parentHeight + windowHeight + offsetTop) / maxParallaxDistanse;
		let parallaxOffset = ((scrollTop + windowHeight) - offsetTop) / distanseStep;

		if (offsetTop < (scrollTop + windowHeight)) {
			parallaxImage.css({
				'transform': 'translateY(' + -parallaxOffset * multiplier + 'px)'
			});
		}

		if (info) {
			console.log("maxParallaxDistanse:" + maxParallaxDistanse);
			console.log("distanseStep:" + distanseStep);
			console.log("parallaxOffset:" + parallaxOffset);
		}
	};

	let windowWidth = $(window).width();

	if (windowWidth > 1200) {
		parallax(".ukraine", 2);
		parallax(".application", 6);

		$(window).scroll(function() {
			parallax(".ukraine", 2);
			parallax(".application", 6);
		});
	}
});