$(document).ready(function() {
	let windowWidth = $(window).width();

	function loadPage() {
		let page = $('.page');
		let header = $('.header');

		page.removeClass('opacity');
		page.addClass('zoomIn');
		header.addClass('header-animation');
	};

	function letterAnimationHeader() {
		let header = $('.header');
		let img = $('.header img.photo');
		let letter = $('.header img.letter');

		let offsetTop = header.offset().top;

		let scrollTop = $(window).scrollTop();
		let windowHeight = $(window).height();

		if (scrollTop > offsetTop && (scrollTop - offsetTop) / 20 < 40) {
			setTimeout((function () {
				img.animate({
					'marginTop': (scrollTop - offsetTop) / 20
				}, 25);
			}), 10);
			setTimeout((function () {
				letter.animate({
					'marginTop': - (scrollTop - offsetTop) / 20
				}, 25);
			}), 10);
		}
	};

	function letterAnimationAbout() {
		let about = $('.about');
		let img = $('.about .left img');
		let letter = $('.about .left p');

		let offsetTop = about.offset().top;

		let scrollTop = $(window).scrollTop();
		let windowHeight = $(window).height();

		if (scrollTop + windowHeight > offsetTop && (scrollTop - offsetTop) / 18 < 50) {
			setTimeout((function () {
				img.animate({
					'marginTop': - (scrollTop - offsetTop) / 18
				}, 25);
			}), 10);
			setTimeout((function () {
				letter.animate({
					'marginTop': (scrollTop - offsetTop) / 18
				}, 25);
			}), 10);
		}
	};

	function imgAnimationBlack() {
		let black = $('.black');
		let why = $('.why');
		let left = $('.black .content .left');
		let letter = $('.black .left p');

		let offsetTop = why.offset().top;
		let heightWhy = why.height();

		let scrollTop = $(window).scrollTop();
		let windowHeight = $(window).height();

		if (scrollTop + windowHeight > (offsetTop + heightWhy / 4) && scrollTop < (offsetTop + heightWhy / 6)) {
			left.css({
				'transform': 'translateY(0)'
			});

			if (windowHeight > 800) {
				setTimeout((function () {
					left.animate({
						'marginTop': - ((scrollTop + windowHeight) - (offsetTop + heightWhy / 4)) / 2.05
					}, 25);
				}), 10);
				setTimeout((function () {
					letter.animate({
						'marginTop': ((scrollTop + windowHeight) - (offsetTop + heightWhy / 4)) / 16
					}, 25);
				}), 10);
			} else {
				setTimeout((function () {
					left.animate({
						'marginTop': - ((scrollTop + windowHeight) - (offsetTop + heightWhy / 4)) / 1.6
					}, 25);
				}), 10);
				setTimeout((function () {
					letter.animate({
						'marginTop': ((scrollTop + windowHeight) - (offsetTop + heightWhy / 4)) / 12
					}, 25);
				}), 10);
			}
		}
	};

	function imgAnimationCar() {
		let car = $('.car');
		let imgLeft = $('.car .right .img-left');
		let imgCenter = $('.car .right .img-center');
		let imgRight = $('.car .right .img-right');

		let offsetTop = car.offset().top;

		let scrollTop = $(window).scrollTop();
		let windowHeight = $(window).height();

		if (scrollTop + windowHeight > offsetTop) {
			setTimeout((function () {
				imgCenter.animate({
					'marginTop': - (scrollTop - offsetTop) / 14
				}, 25);
			}), 10);

			setTimeout((function () {
				imgRight.animate({
					'marginTop': (scrollTop - offsetTop) / 14
				}, 25);
			}), 10);

			setTimeout((function () {
				imgLeft.animate({
					'marginTop': (scrollTop - offsetTop) / 84
				}, 25);
			}), 10);
		}
	};

	function textAnimationWork() {
		let work = $('.work');
		let text = $('.work .right');

		let offsetTop = work.offset().top;

		let scrollTop = $(window).scrollTop();
		let windowHeight = $(window).height();

		if (scrollTop + windowHeight > offsetTop && (scrollTop + windowHeight - offsetTop) / 10 < 200) {
			setTimeout((function () {
				text.animate({
					'paddingTop': (scrollTop + windowHeight - offsetTop) / 10
				}, 25);
			}), 10);
		}
	};

	function letterAnimationQuestions() {
		let questions = $('.questions');
		let letterTop = $('.questions .E');
		let letterBottom = $('.questions .X');

		let offsetTop = questions.offset().top;

		let scrollTop = $(window).scrollTop();
		let windowHeight = $(window).height();

		if (scrollTop + windowHeight > offsetTop) {
			setTimeout((function () {
				letterTop.animate({
					'marginTop': - (scrollTop + windowHeight - offsetTop) / 12
				}, 25);
			}), 10);

			setTimeout((function () {
				letterBottom.animate({
					'marginBottom': - (scrollTop + windowHeight - offsetTop) / 12
				}, 25);
			}), 10);
		}
	};

	setTimeout(loadPage, 1000);

	if (windowWidth > 1200) {
		letterAnimationHeader();
		letterAnimationAbout();
		imgAnimationBlack();
		imgAnimationCar();
		textAnimationWork();
		letterAnimationQuestions();
	}

	$(window).scroll(function() {
		let windowWidth = $(window).width();

		if (windowWidth > 1200) {
			letterAnimationHeader();
			letterAnimationAbout();
			imgAnimationBlack();
			imgAnimationCar();
			textAnimationWork();
			letterAnimationQuestions();
		}
	});
});