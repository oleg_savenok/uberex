$(document).ready(function() {
	let becomeDriver = $('.form-become-driver');
	let openBtn = $('.btn.become-driver');
	let openBtnA = $('a.btn.become-driver li');
	let closeBtn = $('.form-become-driver .btn-close');

	openBtn.click(function() {
		becomeDriver.addClass('open');
	});

	openBtnA.click(function(event) {
		event.preventDefault();
		becomeDriver.addClass('open');
	});

	closeBtn.click(function() {
		becomeDriver.removeClass('open');
	});

	$(document).mouseup(function (e) {
		if (becomeDriver.has(e.target).length === 0){
			becomeDriver.removeClass('open');
		}
	});
});